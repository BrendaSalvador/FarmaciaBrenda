﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia
{
    /// <summary>
    /// Lógica de interacción para Empleado.xaml
    /// </summary>
    public partial class Empleado : Window
    {
        RepositorioDeEmpleado repositorio;

        bool esNuevo;

        public string NumEmpleado { get; private set; }

        public Empleado()
        {
            InitializeComponent();
        }
        private void HabilitarCajas(bool habilitadas)
        {

            txbNumEmpleado.Clear();
            txbNombres.Clear();

            txbNumEmpleado.IsEnabled = habilitadas;
            txbNombres.IsEnabled = habilitadas;


        }

        private void HabilitarBotones(bool habilitados)
        {
            btnNuevo.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = habilitados;

        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbNombres.Text) || string.IsNullOrEmpty(txbNumEmpleado.Text) || string.IsNullOrEmpty(txbNumEmpleado.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Empleado a = new Empleado()
                {

                    NumEmpleado= txbNumEmpleado.Text,
                    Name = txbNombres.Text,

                };
                if (repositorio.Agregar(a))
                {
                    MessageBox.Show("Guardado con Éxito", "empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Empleado original = dtgTabla.SelectedItem as Empleado;
                Empleado a = new Empleado();

                a.Name = txbNumEmpleado.Text;
                a.Name = txbNombres.Text;

                if (repositorio.ModificarEmpleado(original, a)) 
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("Su empleado a sido actualizado", "empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu empleado", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void ActualizarTabla()
        {
            dtgTabla.ItemsSource = null;
            dtgTabla.ItemsSource = repositorio.LeerEmpleado();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerEmpleado().Count == 0)
            {
                MessageBox.Show("Nadien quiero ser empleado..", "No tienes empleados", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    Empleado a = dtgTabla.SelectedItem as Empleado;
                    if (MessageBox.Show("Realmente deseas eliminar a " + a.ContentStringFormat + "?", "Eliminar????", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.EliminarEmpleado(a))
                        {
                            MessageBox.Show("Tu empleado ha sido removido", "empleado", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar a tu empleado, ", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "empleado", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.LeerEmpleado().Count == 0)
            {
                MessageBox.Show("Nadie  quiere ser tu empleado...", "No tienes empleados", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (dtgTabla.SelectedItem != null)
                {
                    Empleado a = dtgTabla.SelectedItem as Empleado;
                    HabilitarCajas(true);
                    txbNumEmpleado.Text = a.Name;
                    txbNombres.Text = a.Name;

                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Empleado", MessageBoxButton.OK, MessageBoxImage.Question);
                }


            }
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {


            this.Hide();
            MainWindow principal = new MainWindow();
            principal.Owner = this;
            principal.Show();
        }
    }
}

  

 
    
