﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia
{
    class RepositorioDeEmpleado
    {
        ManejadorDeArchivos archivoEmpleado;
        List<Empleados> Empleado;
        private List<Empleados> empleado;

        public RepositorioDeEmpleado()
        {
            archivoEmpleado = new ManejadorDeArchivos("empleado.txt");
            Empleado = new List<Empleados>();
        }

        public bool AgregarAmigo(Empleados empleado)
        {
            Empleado.Add(empleado);
            bool resultado = ActualizarArchivo();
            Empleado = LeerEmpleado();
            return resultado;
        }

        public bool EliminarCliente(Empleados empleado)
        {
            Empleados temporal = new Empleados();
            foreach (var item in Empleado)
            {
                if (item.Nombres == empleado.Nombres)
                {
                    temporal = item;
                }
            }
            Empleado.Remove(temporal);
            bool resultado = ActualizarArchivo();
            Empleado = LeerEmpleado();
            return resultado;
        }

        public bool ModificarEmpleado(Empleados original, Empleados modificado)
        {
            Empleados temporal = new Empleados();
            foreach (var item in Empleado)
            {
                if (original.Nombres == item.Nombres)
                {
                    temporal = item;
                }
            }
            temporal.Nombres = modificado.Nombres;
            temporal.NumEmpleado = modificado.NumEmpleado;
           
            bool resultado = ActualizarArchivo();
            Empleado = LeerEmpleado();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string datos = "";
            foreach (Empleados item in Empleado)
            {
                datos += string.Format("{0}|{1}\n", item.Nombres, item.NumEmpleado);
            }
            return archivoEmpleado.Guardar(datos);
        }
        public List<Empleados> LeerEmpleado()
        {
            string datos = archivoEmpleado.Leer();
            if (datos != null)
            {
                List<Empleados> cliente = new List<Empleados>();
                string[] lineas = datos.Split('\n');
                for (int i = 0; i < lineas.Length - 1; i++)
                {
                    string[] campos = lineas[i].Split('|');
                    Empleados a = new Empleados()
                    {
                        Nombres = campos[0],
                        NumEmpleado = campos[1],
                        
                    };
                    this.Empleado.Add(a);
                }
                this.Empleado = empleado;
                return this.Empleado;
            }
            else
            {
                return null;
            }
        }

        internal bool Agregar(Empleado a)
        {
            throw new NotImplementedException();
        }

        internal bool ModificarEmpleado(Empleado original, Empleado a)
        {
            throw new NotImplementedException();
        }

        internal bool EliminarEmpleado(Empleado a)
        {
            throw new NotImplementedException();
        }
    }

}
   
