﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmacia
{
    class RepositorioDeClientes
    {
        ManejadorDeArchivos archivoClientes;
        List<Clientes> Cliente;
        private List<Clientes> cliente;


        public RepositorioDeClientes()
        {
            archivoClientes = new ManejadorDeArchivos("clientes.txt");
            Cliente = new List<Clientes>();
        }

        public bool AgregarAmigo(Clientes cliente)
        {
            Cliente.Add(cliente);
            bool resultado = ActualizarArchivo();
            Cliente = LeerCliente();
            return resultado;
        }

        public bool EliminarCliente(Clientes cliente)
        {
            Clientes temporal = new Clientes();
            foreach (var item in Cliente)
            {
                if (item.Telefono == cliente.Telefono)
                {
                    temporal = item;
                }
            }
            Cliente.Remove(temporal);
            bool resultado = ActualizarArchivo();
            Cliente = LeerCliente();
            return resultado;
        }

        public bool ModificarCliente(Clientes original, Clientes modificado)
        {
            Clientes temporal = new Clientes();
            foreach (var item in Cliente)
            {
                if (original.Telefono == item.Telefono)
                {
                    temporal = item;
                }
            }
            temporal.Nombre = modificado.Nombre;
            temporal.RFC = modificado.RFC;
            temporal.Correo = modificado.Correo;
            temporal.Telefono = modificado.Telefono;
            temporal.Direccion = modificado.Direccion;
            bool resultado = ActualizarArchivo();
            Cliente = LeerCliente();
            return resultado;
        }

        private bool ActualizarArchivo()
        {
            string datos = "";
            foreach (Clientes item in Cliente)
            {
                datos += string.Format("{0}|{1}|{2}|{3}|{4}\n", item.Nombre, item.RFC, item.Correo, item.Telefono, item.Direccion);
            }
            return archivoClientes.Guardar(datos);
        }
        public List<Clientes> LeerCliente()
        {
            string datos = archivoClientes.Leer();
            if (datos != null)
            {
                List<Clientes> cliente = new List<Clientes>();
                string[] lineas = datos.Split('\n');
                for (int i = 0; i < lineas.Length - 1; i++)
                {
                    string[] campos = lineas[i].Split('|');
                    Clientes a = new Clientes()
                    {
                        Nombre = campos[0],
                        RFC = campos[1],
                        Correo = campos[2],
                        Telefono = campos[3],
                        Direccion = campos[4]
                    };
                    this.Cliente.Add(a);
                }
                this.Cliente = cliente;
                return this.Cliente;
            }
            else
            {
                return null;
            }
        }

        internal bool EliminarCliente(Cliente c)
        {
            throw new NotImplementedException();
        }
    }
    
}
